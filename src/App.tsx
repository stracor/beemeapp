import React from 'react';

//Third party libraries
import { Provider } from 'react-redux';

//Store
import store from '@store/index';

//Pages
import RankPage from '@pages/RankPage';

const App = () => {
	return (
		<Provider store={store}>
			<RankPage />
		</Provider>
	);
};

export default App;
