import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
	users: [],
	loading: false,
	error: '',
	page: 1,
};

export const fetchUser = createAsyncThunk('fetchUser', async (reset, { getState }) => {
	const state = getState();

	const res = await axios.get('https://randomuser.me/api/', {
		params: {
			results: 20,
			page: reset ? 1 : state.user.page,
		},
	});

	return res.data;
});

const userSlice = createSlice({
	name: 'users',
	initialState,
	reducers: {},
	extraReducers: builder => {
		builder.addCase(fetchUser.pending, state => {
			state.loading = true;
			state.error = '';
		});
		builder.addCase(fetchUser.fulfilled, (state, action) => {
			if (action?.payload?.info?.page == 1) {
				state.users = action.payload.results;
			} else {
				state.users = [...state.users, ...action.payload.results];
			}
			state.loading = false;
			state.page = action?.payload?.info?.page + 1;
		});

		builder.addCase(fetchUser.rejected, state => {
			state.loading = false;
			state.error = 'Hata Oluştu';
			state.page = 1;
		});
	},
});

export default userSlice.reducer;
