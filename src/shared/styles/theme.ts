export const colors = {
	grey: '#F8F5F5',
	darkGrey: '#e6e6e6',
	textGrey: '#b7b7b7',
	textDarkGrey: '#5b5b5b',
	white: '#FFFFFF',
	bisque: '#FEEBC3',
	yellow: '#F6BB4A',
};
