import React from 'react';
import { TouchableOpacity, View } from 'react-native';

//Atoms
import CircleRank from '@components/atoms/CircleRank';
import Name from '@components/atoms/Name';
import ProfileIcon from '@components/atoms/ProfileIcon';
import Cup from '@components/atoms/Cup';

//Images
import Bgg from '@assets/images/Mask Group 26_g.svg';
import Bgc from '@assets/images/Mask Group 26_c.svg';

//Styles
import styles from './style';

import { IRankCard } from './type';

const RankCard = ({ rank, name, userName, image, onPress = () => {} }: IRankCard) => {
	const Bg = () => (rank == 1 ? <Bgc style={styles.bgImage} /> : <Bgg style={styles.bgImage} />);

	return (
		<TouchableOpacity onPress={onPress} style={[styles.container, rank > 3 ? styles.unRankedContainer : styles.rankedContainer(rank)]}>
			{rank <= 3 && <Bg />}

			<CircleRank rank={rank} containerStyle={{ marginRight: 20 }} />
			<ProfileIcon rank={rank} containerStyle={{ marginRight: 10 }} image={image} />
			<Name name={name} userName={userName} rank={rank} containerStyle={{ flex: 1, marginRight: 20 }} />

			<View style={{ alignContent: 'flex-end' }}>
				<Cup rank={rank} />
			</View>
		</TouchableOpacity>
	);
};

export default RankCard;
