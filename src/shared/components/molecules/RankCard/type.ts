import { GestureResponderEvent } from 'react-native';

import { ICup } from './../../atoms/Cup/type';
import { IProfileIcon } from './../../atoms/ProfileIcon/type';
import { IName } from './../../atoms/Name/type';
import { ICircleRank } from './../../atoms/CircleRank/type';

export type IRankCard = {
	onPress: (event: GestureResponderEvent) => void;
} & Omit<ICircleRank, 'containerStyle'> &
	Omit<IProfileIcon, 'containerStyle'> &
	Omit<IName, 'containerStyle'> &
	Omit<ICup, 'containerStyle'>;
