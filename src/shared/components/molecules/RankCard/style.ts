import { StyleSheet } from 'react-native';
import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		marginVertical: 10,
		marginHorizontal: 20,
		padding: 14,
		borderRadius: 10,
		alignItems: 'center',
	},
	unRankedContainer: {
		borderWidth: 2,
		borderColor: '#f4f4f4',
	},
	rankedContainer: rank => ({
		backgroundColor: style[rank]?.backgroundColor,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 7,
		},
		shadowOpacity: 0.41,
		shadowRadius: 9.11,

		elevation: 5,
	}),
	bgImage: {
		position: 'absolute',
	},
});

const style = {
	1: {
		backgroundColor: '#feebc1',
		backgroundOpacity: 1,
	},
	2: {
		backgroundColor: '#ffffff',
		backgroundOpacity: 0.7,
	},
	3: {
		backgroundColor: '#f9f5f4',
		backgroundOpacity: 0.5,
	},
};

export default styles;
