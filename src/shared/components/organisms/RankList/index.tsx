import React, { useEffect, useState } from 'react';
import { Alert, FlatList, RefreshControl, Text } from 'react-native';

//Third party libraries
import { useDispatch, useSelector } from 'react-redux';

//Types
import { User } from '@models/types';

//Molecules
import RankCard from '@components/molecules/RankCard';
import { fetchUser } from '@store/slices/userSlice';

const RankList = () => {
	const userSlice = useSelector(state => state.user);
	const dispatch = useDispatch();
	const { users = [], page = 1, loading = false } = userSlice;

	useEffect(() => {
		loadMore(true)();
	}, []);

	const showDetail = (item: User, rank: number) => () =>
		Alert.alert(
			`#${rank} ${item.name.first} ${item.name.last}`,
			`Kullanıcı Adı: ${item?.login?.username} \nMail: ${item?.email}\nÜlke: ${item?.location?.country}`,
		);

	const renderItem = ({ item, index }) => (
		<RankCard
			onPress={showDetail(item, index + 1)}
			name={`${item?.name?.first} ${item?.name?.last}`}
			rank={index + 1}
			image={item?.picture?.thumbnail}
			userName={item?.login?.username}
		/>
	);

	const keyExtractor = i => i?.login?.uuid;

	const loadMore =
		(refresh = false) =>
		() =>
			(page < 6 || refresh) && dispatch(fetchUser(refresh));

	return (
		<FlatList
			contentContainerStyle={{ paddingTop: 20 }}
			refreshControl={<RefreshControl refreshing={loading} onRefresh={loadMore(true)} />}
			data={users}
			keyExtractor={keyExtractor}
			renderItem={renderItem}
			onEndReachedThreshold={0.01}
			onEndReached={loadMore()}
		/>
	);
};

export default RankList;
