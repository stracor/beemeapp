import React from 'react';

import Gold from '@assets/images/Component 5 – 1.svg';
import Silver from '@assets/images/Component 6 – 1.svg';
import Bronze from '@assets/images/Component 7 – 1.svg';

import { ICup } from './type';

const Cup = ({ rank }: ICup) => {
	switch (rank) {
		case 1:
			return <Gold />;
		case 2:
			return <Silver />;
		case 3:
			return <Bronze />;
		default:
			return null;
	}
};

export default Cup;
