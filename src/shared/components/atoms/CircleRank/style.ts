import { colors } from '@styles/theme';
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
	container: {
		width: 35,
		height: 35,
		borderRadius: 40,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.darkGrey,
	},
	rankText: {
		fontWeight: 'bold',
		fontSize: 20,
	},
	rank: {
		backgroundColor: colors.yellow,
	},
});

export default styles;
