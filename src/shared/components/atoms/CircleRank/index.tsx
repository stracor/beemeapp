import React from 'react';
import { Text, View } from 'react-native';

//Styles and services
import styles from './style';

import { ICircleRank } from './type';

const CircleRank = ({ rank = 1, containerStyle = {} }: ICircleRank) => {
	return (
		<View style={[styles.container, rank <= 3 && styles.rank, containerStyle]}>
			<Text numberOfLines={1} adjustsFontSizeToFit style={styles.rankText}>
				{rank}
			</Text>
		</View>
	);
};

export default CircleRank;
