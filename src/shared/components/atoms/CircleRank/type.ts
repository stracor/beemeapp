import { StyleProp, ViewStyle } from 'react-native';

export type ICircleRank = {
	rank: number;
	containerStyle?: StyleProp<ViewStyle>;
};
