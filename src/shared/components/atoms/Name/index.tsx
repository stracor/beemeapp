import React from 'react';
import { Text, View } from 'react-native';

//Styles and services
import styles from './style';

import { IName } from './type';

const Name = ({ name = '-', userName = '-', rank = 3, containerStyle = {} }: IName) => {
	return (
		<View style={[styles.container, containerStyle]}>
			<Text style={styles.nameText}>{name.toLowerCase()}</Text>
			<Text style={[styles.userNameText, rank <= 3 && styles.rankUserName]}>{userName.toLowerCase()}</Text>
		</View>
	);
};

export default Name;
