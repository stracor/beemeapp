import { StyleSheet } from 'react-native';
import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {},
	nameText: {
		fontWeight: '600',
		fontSize: 14,
		marginBottom: -5,
	},
	userNameText: {
		fontWeight: '400',
		fontSize: 14,
		color: colors.textGrey,
	},
	rankUserName: {
		color: colors.textDarkGrey,
	},
});

export default styles;
