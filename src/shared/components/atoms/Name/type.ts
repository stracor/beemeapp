import { StyleProp, ViewStyle } from 'react-native';

export type IName = {
	name: string;
	userName: string;
	rank: number;
	containerStyle?: StyleProp<ViewStyle>;
};
