import { colors } from '@styles/theme';
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
	container: {
		width: 65,
		height: 65,
		borderRadius: 99,
		borderWidth: 4,
		alignItems: 'center',
		justifyContent: 'center',
		borderColor: colors.darkGrey,
		overflow: 'hidden',
	},
	rankText: {
		fontWeight: 'bold',
		fontSize: 20,
	},
	rank: {
		borderStyle: 'dotted',
		borderColor: colors.yellow,
	},
	image: {
		width: 58,
		height: 58,
		borderRadius: 99,
	},
});

export default styles;
