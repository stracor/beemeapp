import { StyleProp, ViewStyle } from 'react-native';

export type IProfileIcon = {
	image: string;
	rank: number;
	containerStyle?: StyleProp<ViewStyle>;
};
