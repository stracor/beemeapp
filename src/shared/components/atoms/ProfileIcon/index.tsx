import React from 'react';
import { Image, View } from 'react-native';

//Styles and services
import styles from './style';

import { IProfileIcon } from './type';

const ProfileIcon = ({
	image = 'https://icon-library.com/images/no-user-image-icon/no-user-image-icon-8.jpg',
	rank = 1,
	containerStyle = {},
}: IProfileIcon) => {
	return (
		<View style={[styles.container, rank <= 3 && styles.rank, containerStyle]}>
			<Image style={styles.image} source={{ uri: image }} />
		</View>
	);
};

export default ProfileIcon;
