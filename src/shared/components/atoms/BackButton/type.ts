import { GestureResponderEvent } from 'react-native';

export type IBackButton = {
	/**
	 * Butona basıldığında çalışacak event.
	 */
	onPress: (event: GestureResponderEvent) => void;
};
