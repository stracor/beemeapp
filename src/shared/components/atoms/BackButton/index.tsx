import React from 'react';
import { Pressable, View } from 'react-native';

//Assets
import Arrow from '@assets/images/Arrow-icon.svg';

import { IBackButton } from './type';

//Styles
import styles from './style';

const BackButton = ({ onPress }: IBackButton) => {
	return (
		<Pressable onPress={onPress} style={styles.container}>
			<Arrow />
		</Pressable>
	);
};

export default BackButton;
