import { StyleSheet } from 'react-native';
import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		borderRadius: 10,
		width: 40,
		height: 40,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'white',
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,

		elevation: 5,
	},
});

export default styles;
