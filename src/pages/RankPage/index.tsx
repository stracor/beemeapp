import React from 'react';
import { Alert, Text, View } from 'react-native';

//Components
import RankList from '@components/organisms/RankList';
import BackButton from '@components/atoms/BackButton';

//Styles
import styles from './style';

const RankPage = () => {
	const alert = () => Alert.alert('Az kaldı', 'Gerisi yakında');

	return (
		<View style={styles.container}>
			<View style={styles.headerContainer}>
				<BackButton onPress={alert} />
				<Text style={styles.title}>Sıralama</Text>
				<View style={styles.empty} />
			</View>
			<RankList />
		</View>
	);
};

export default RankPage;
