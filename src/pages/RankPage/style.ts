import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	headerContainer: {
		backgroundColor: 'white',
		flexDirection: 'row',
		padding: 15,
		shadowColor: '#000',
		justifyContent: 'space-between',
		alignItems: 'center',
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,

		elevation: 5,
	},
	title: {
		fontWeight: '500',
		fontSize: 15,
	},
	empty: {
		width: 40,
	},
});

export default styles;
