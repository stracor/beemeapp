module.exports = {
	presets: ['module:metro-react-native-babel-preset'],
	plugins: [
		[
			'module-resolver',
			{
				root: ['./src'],
				extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
				alias: {
					'@assets': './src/assets',
					'@pages': './src/pages',
					'@components': './src/shared/components',
					'@store': './src/shared/store',
					'@styles': './src/shared/styles',
					'@models': './src/shared/models',
				},
			},
		],
	],
};
